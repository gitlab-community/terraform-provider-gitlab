---
page_title: "GitLab Provider"
subcategory: ""
description: |-
  Terraform Provider for GitLab
---

Use the GitLab provider to interact with GitLab resources, like
users, groups, projects and more. You must configure the provider with
the proper credentials before you can use it.

The provider uses the [`client-go`](https://gitlab.com/gitlab-org/api/client-go) library
to interact with the [GitLab REST API](https://docs.gitlab.com/api/api_resources/).

We support the following versions:

- Latest 3 patch releases within a major release. For example, if current is 17.8, we support 17.6-17.8. Or if current is 18.1, we support 18.0-18.1.
- We introduce any breaking changes on major releases only. For example, 17.0 or 18.0.
- We run tests against the latest 3 patch releases regardless of whether these cross a major release boundary. For example, if current is 17.8, we test 17.6-17.8. Or if current is 18.1, we test 17.11-18.1.

All other versions are best effort support.

-> Note, that the compatibility between a provider release and GitLab itself **cannot** be inferred from the
release version. New features added to GitLab may not be added to the provider until later versions.
Equally, features removed or deprecated in GitLab may not be removed or deprecated from the provider until later versions.

Each data source and resource references the appropriate upstream GitLab REST API documentation,
which may be consumed to better understand the behavior of the API.

Use the navigation to the left to read about the valid data sources and resources.

This provider requires at least [Terraform 1.0](https://www.terraform.io/downloads.html).
A minimum of Terraform 1.4.0 is recommended.

-> Using a Project or Group access token may cause issues with some resources, as those token types don't 
have full access to every API. This is also true when using a `CI_JOB_TOKEN`. Consider using a dedicated
Personal Access Token or Service Account if you are experiencing permission errors when adding resources. 

## Example Usage

{{tffile "examples/provider/provider.tf"}}

{{ .SchemaMarkdown | trimspace }}
