# GitLab group security policy attachments can be imported using an id made up of `group:policy_project_id` where the policy project ID is the project ID of the policy project, e.g.
terraform import gitlab_group_security_policy_attachment.foo 1:2
