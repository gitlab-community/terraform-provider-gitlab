# You can import a group service account using `terraform import <resource> <id>`.  The
# `id` is the id of the service account
terraform import gitlab_instance_service_account.example example
